﻿# Leogrid - ITSM & CMDB
 
Leogrid stands for *IT Operations Portal*.
It is a complete open source, ITIL, web based service management tool including a fully customizable CMDB, a helpdesk system and a document management tool. 
Leogrid also offers mass import tools and web services to integrate with your IT


## Features
- Fully configurable **CMDB**
- **HelpDesk** and Incident Management
- **Service and Contract Management**
- **Change** Management
- **Configuration** Management
- Automatic **SLA** management
- Automatic **impact analysis**
- **CSV import** tool for all data
- Consistency **audit** to check data quality
- **Data synchronization** (for data federation)





   
 


