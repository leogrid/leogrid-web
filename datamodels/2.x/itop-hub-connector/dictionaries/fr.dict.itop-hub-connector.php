<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */
Dict::Add('FR FR', 'French', 'Français', array(
	// Dictionary entries go here
	'Menu:iTopHub' => 'Leogrid Hub',
	'Menu:iTopHub:Register' => ' ',
	'Menu:iTopHub:Register+' => 'Connectez-vous à Leogrid Hub pour enregistrer cette instance d\'iTop',
	'Menu:iTopHub:Register:Description' => '<p>Connectez-vous à la communauté Leogrid Hub!</br>Trouvez tout le contenu dont vous avez besoin, gérer vos instances d\'iTop depuis un tableau de bord centralisé et déployez de nouvelles extensions.</br><br/>En vous connectant au Hub depuis cette page, vous transmettez au Hub des informations relatives à cette instance d\'iTop.</p>',
	'Menu:iTopHub:MyExtensions' => ' ',
	'Menu:iTopHub:MyExtensions+' => 'Voir la liste des extensions déployes sur cette instance',
	'Menu:iTopHub:BrowseExtensions' => '  ',
	'Menu:iTopHub:BrowseExtensions+' => 'Parcourir la listes des extensions disponibles sur Leogrid Hub',
	'Menu:iTopHub:BrowseExtensions:Description' => '<p>Découvrez le magasin d\'extensions Leogrid Hub !</br>Trouvez en quelques clics celles qui vous permettront de construire un iTop sur mesure qui se conforme à vos processus.</br><br/>En vous connectant au Hub depuis cette page, vous transmettez au Hub des informations relatives à cette instance d\'iTop.</p>',
	'iTopHub:GoBtn' => 'Aller sur Leogrid Hub',
	'iTopHub:CloseBtn' => 'Fermer',
	'iTopHub:GoBtn:Tooltip' => 'Naviguer vers www.itophub.io',
	'iTopHub:OpenInNewWindow' => 'Ouvrir Leogrid Hub dans une nouvelle fenêtre',
	'iTopHub:AutoSubmit' => 'Ne plus me demander. La prochaine fois, aller sur Leogrid Hub automatiquement.',
	'UI:About:RemoteExtensionSource' => 'Leogrid Hub',
	'iTopHub:Explanation' => 'En cliquant sur ce bouton, vous serez redirigé vers Leogrid Hub.',

	'iTopHub:BackupFreeDiskSpaceIn' => '%1$s d\'espace disque disponible sur %2$s.',
	'iTopHub:FailedToCheckFreeDiskSpace' => 'Echec de la vérification de l\'espace disque.',
	'iTopHub:BackupOk' => 'Sauvegarde Ok.',
	'iTopHub:BackupFailed' => 'Echec de la sauvegarde !',
	'iTopHub:Landing:Status' => 'Etat du déploiement',
	'iTopHub:Landing:Install' => 'Déploiement des extensions...',
	'iTopHub:CompiledOK' => 'Compilation réussie.',
	'iTopHub:ConfigurationSafelyReverted' => 'Une erreur a été détectée durant le déploiement!<br/>La configuration d\'iTop n\'a PAS été modifiée.',
	'iTopHub:FailAuthent' => 'Échec d\'authentification pour cette action',

	'iTopHub:InstalledExtensions' => '  sur cette instance',
	'iTopHub:ExtensionCategory:Manual' => '  manuellement',
	'iTopHub:ExtensionCategory:Manual+' => 'Les extensions ci-dessous ont été déployées en les copiant manuellement dans le répertoire %1$s d\'iTop:',
	'iTopHub:ExtensionCategory:Remote' => '  depuis Leogrid Hub',
	'iTopHub:ExtensionCategory:Remote+' => 'Les extensions ci-dessous ont été déployées depuis Leogrid Hub:',
	'iTopHub:NoExtensionInThisCategory' => 'Il n\'y a pas d\'extension dans cette catégorie<br/><br>Avec Leogrid Hub trouvez en quelques clics les extensions qui vous permettront de construire un iTop sur mesure qui se conforme à vos processus.',
	'iTopHub:ExtensionNotInstalled' => 'Non installée',
	'iTopHub:GetMoreExtensions' => '  ...',

	'iTopHub:LandingWelcome' => 'Félicitations! Les extensions ci-dessous ont été téléchargées depuis Leogrid Hub et installées sur cette instance d\'iTop.',
	'iTopHub:GoBackToITopBtn' => 'Retourner dans iTop',
	'iTopHub:Uncompressing' => 'Décompression des extensions...',
	'iTopHub:InstallationWelcome' => 'Installation des extensions téléchargées depuis Leogrid Hub',
	'iTopHub:DBBackupLabel' => 'Sauvegarde de l\'instance iTop',
	'iTopHub:DBBackupSentence' => 'Faire une sauvegarde de la base de données et des paramétrages d\'iTop',
	'iTopHub:DeployBtn' => 'Déployer !',
	'iTopHub:DatabaseBackupProgress' => 'Sauvegarde de l\'instance...',

	'iTopHub:InstallationEffect:Install' => 'Version: %1$s sera installée.',
	'iTopHub:InstallationEffect:NoChange' => 'Version: %1$s déjà installée. Rien ne changera.',
	'iTopHub:InstallationEffect:Upgrade' => 'Sera <b>mise à jour</b> de version %1$s en version %2$s.',
	'iTopHub:InstallationEffect:Downgrade' => 'Sera <b>DEGRADEE</b> de version %1$s en version %2$s.',
	'iTopHub:InstallationProgress:DatabaseBackup' => 'Sauvegarde de l\'instance iTop...',
	'iTopHub:InstallationProgress:ExtensionsInstallation' => 'Installation des extensions',
	'iTopHub:InstallationEffect:MissingDependencies' => 'Cette extension ne peut pas être installée à cause de ses dépendences.',
	'iTopHub:InstallationEffect:MissingDependencies_Details' => 'Cette extension nécessite le(s) module(s): %1$s',
	'iTopHub:InstallationProgress:InstallationSuccessful' => 'Installation réussie !',

	'iTopHub:InstallationStatus:Installed_Version' => '%1$s version: %2$s.',
	'iTopHub:InstallationStatus:Installed' => 'Installée',
	'iTopHub:InstallationStatus:Version_NotInstalled' => 'Version %1$s <b>NON</b> installée.',
));


