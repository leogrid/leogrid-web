
# Leogrid - ITSM & CMDB

It is a complete open source, ITIL, web based service management tool including a fully customizable CMDB, a helpdesk system and a document management tool.
It also offers mass import tools and web services to integrate with your IT

## Features

-   Fully configurable [Configuration Management (CMDB)][10]
-   [HelpDesk][11] and Incident Management
-   [Service and Contract Management][12]
-   [Change][13] Management
-   Configurable [SLA][14] Management
-   Graphical [impact analysis][15]
-   [CSV import][16] tool for any data
-   Consistency [audit][17] to check data quality
-   [Data synchronization][18] (for data federation)

## Latest release

-   [Changes since the previous version][62]
-   [New features][63]
-   [Installation notes][64]
-   [Download][65]




